#pragma once
#include "Tank.h"
#include <iostream>
#include <vector>
#include "OutputFile.h"
using namespace std;

class Engine {
	double fuel_per_second;
	bool status;
	int selected_tank_id;
	vector<Tank> tank;
public:
	string output;
	Engine();
	~Engine();
	void consume_fuel_per_second();
	void start_engine();
	void stop_engine();
	void give_back_fuel(int quantity);
	void add_fuel_tank(int capacity);
	void list_fuel_tanks();
	void print_fuel_tank_count();
	void remove_fuel_tank(int tank_id);
	void connect_fuel_tank_to_engine(int tank_id);
	void disconnect_fuel_tank_from_engine(int tank_id);
	void list_connected_tanks();
	void print_total_fuel_quantity();
	void print_total_consumed_fuel_quantity();
	void print_tank_info(int tank_id);
	void fill_tank(int tank_id, double fuel_quantity);
	void open_valve(int tank_id);
	void close_valve(int tank_id);
	void break_fuel_tank(int tank_id);
	void repair_fuel_tank(int tank_id);
	void wait(int seconds);
};