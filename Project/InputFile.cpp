#include "InputFile.h"
#include "Engine.h"

void InputFile::ReadFile(string fileName, Engine& engine) {
	ifstream file(fileName);
	string str = "";
	if (file.is_open()) {
		while (!file.eof()) {
			getline(file, str);
			commands(str, engine);
		}
		file.close();
	}
	else {
		cout << "failed to read the file." << endl;
	}
}

void InputFile::commands(string str, Engine& engine) {//edit it
	size_t comma = str.find(';');
	string sub = str.substr(0, comma);
	size_t space = str.find(' ');
	string sub2 = sub.substr(0, space);
	engine.consume_fuel_per_second();
	if (sub2 == "start_engine") { engine.start_engine(); }// 
	else if (sub2 == "stop_engine") { engine.stop_engine(); }// 
	else if (sub2 == "give_back_fuel") {//<>
		size_t s = sub.find('>');
		string q = sub.substr(space + 2, s - space - 2);
		int quantity = 0;//stoi(q);
		//engine.give_back_fuel(quantity);
	}
	else if (sub2 == "add_fuel_tank") {// int
		string q = sub.substr(space + 1, sub.size() - space - 1);
		int quantity = stoi(q);
		engine.add_fuel_tank(quantity);
	}
	else if (sub2 == "list_fuel_tanks") { engine.list_fuel_tanks(); }//
	else if (sub2 == "print_fuel_tank_count") { engine.print_fuel_tank_count(); }
	else if (sub2 == "remove_fuel_tank") {// int
		string q = sub.substr(space + 1, sub.size() - space - 1);
		int id = stoi(q);
		engine.remove_fuel_tank(id);
	}
	else if (sub2 == "connect_fuel_tank_to_engine") {// int
		string q = sub.substr(space + 1, sub.size() - space - 1);
		int id = stoi(q);
		engine.connect_fuel_tank_to_engine(id);
	}
	else if (sub2 == "disconnect_fuel_tank_from_engine") {// int
		string q = sub.substr(space + 1, sub.size() - space - 1);
		int id = stoi(q);
		engine.disconnect_fuel_tank_from_engine(id);
	}
	else if (sub2 == "list_connected_tanks") { engine.list_connected_tanks(); }//
	else if (sub2 == "print_total_fuel_quantity") { engine.print_total_fuel_quantity(); }//
	else if (sub2 == "print_total_consumed_fuel_quantity") { engine.print_total_consumed_fuel_quantity(); }//
	else if (sub2 == "print_tank_info") {// int
		string q = sub.substr(space + 1, sub.size() - space - 1);
		int id = stoi(q);
		engine.print_tank_info(id);
	}
	else if (sub2 == "fill_tank") {// int int
		string q = sub.substr(space + 1, comma - space - 1);
		size_t s = q.find(' ');
		string first, last;
		first = q.substr(0, s);
		last = q.substr(s + 1, q.length() - s - 1);

		int id = stoi(first);
		int quantity = stoi(last);
		engine.fill_tank(id, quantity);
	}
	else if (sub2 == "open_valve") {// int
		string q = sub.substr(space + 1, sub.size() - space - 1);
		int id = stoi(q);
		engine.open_valve(id);
	}
	else if (sub2 == "close_valve") {//<>
		string q = sub.substr(space + 2, sub.size() - space - 2);
		int id = 0;//stoi(q);
		//engine.close_valve(id);
	}
	else if (sub2 == "break_fuel_tank") {// int
		string q = sub.substr(space + 1, sub.size() - space - 1);
		int id = stoi(q);
		engine.break_fuel_tank(id);
	}
	else if (sub2 == "repair_fuel_tank") {// int
		string q = sub.substr(space + 1, sub.size() - space - 1);
		int id = stoi(q);
		engine.repair_fuel_tank(id);
	}
	else if (sub2 == "wait") {// int
		string q = sub.substr(space + 1, sub.size() - space - 1);
		int seconds = stoi(q);
		engine.wait(seconds);
	}
	else if (sub2 == "stop_simulation") { stop_simulation(engine); }// 
	else {
		cout << "Incorrect command" << endl;
	}
}

void InputFile::stop_simulation(Engine& engine) {
	string quitMsg = "";
	int tankCount = 3;
	quitMsg += "Engine: Simulation stopped\n";
	for (int i = 0; i < tankCount; i++) {
		quitMsg += "Tank " + to_string((i + 1)) + ": Simulation stopped \n";
		quitMsg += "Valve " + to_string((i + 1)) + ": Simulation stopped \n";
	}
	cout << "Simulation Stoped" << endl;
	engine.output += quitMsg;
}