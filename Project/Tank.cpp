#include "Tank.h"

Tank::Tank(double _capacity) {
	capacity = _capacity;
	fuel_quantity = 0;
	broken = false;
	valve = false;
	connection = false;
}
Tank::~Tank() {

}
double Tank::getCapacity() {
	return capacity;
}
double Tank::getFuelQuantity() {
	return fuel_quantity;
}
bool Tank::getValve() {
	return valve;
}
bool Tank::getConnection() {
	return connection;
}
void Tank::setFuelQuantity(double _fuel_quantity) {
	fuel_quantity += _fuel_quantity;
}
void Tank::setBroken(bool isBroken) {
	broken = isBroken;
}
void Tank::setValve(bool valveOpen) {
	valve = valveOpen;
}
void Tank::setConnection(bool _connection) {
	connection = _connection;
}
string Tank::getInfo() {
	string info = "";
	info += "Capacity : " + to_string(capacity) + "\n";
	info += "Fuel Quantity : " + to_string(fuel_quantity) + "\n";
	info += "Broken : " + to_string(broken) + "\n";
	info += "Valve : " + to_string(valve) + "\n";
	return info;
}