#include "OutputFile.h"
#include "Engine.h"

OutputFile::OutputFile() {
	this->to_write = "";
}

void OutputFile::WriteFile(string fileName) {
	ofstream file(fileName);
	if (file.is_open()) {
		file.clear();
		file << this->to_write << endl;
		file.close();
	}
	else {
		cout << "failed to write the file." << endl;
	}
}