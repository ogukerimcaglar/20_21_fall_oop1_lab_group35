#include <iostream>
#include "Engine.h"

Engine::Engine() {
	this->output = "";
	this->fuel_per_second = 5.5f;
	this->status = false;
	this->selected_tank_id = 0;
	Tank internalTank(55.0f);
	internalTank.setFuelQuantity(55.0f);
	internalTank.setValve(true);
	internalTank.setConnection(true);
	this->tank = vector <Tank>();
	this->tank.push_back(internalTank);
}
Engine::~Engine() {
	tank.clear();
}
void Engine::consume_fuel_per_second() {
	if (this->status == true) {
		if (this->selected_tank_id == 0 && this->tank[0].getFuelQuantity() >= 20) {
			this->tank[0].setFuelQuantity(-this->fuel_per_second);
			return;
		}
		else if ((this->selected_tank_id == 0 && this->tank[0].getFuelQuantity() < 20) ||
			(this->tank[this->selected_tank_id].getFuelQuantity() <= 0)) {
			this->tank[this->selected_tank_id].setFuelQuantity(-this->tank[this->selected_tank_id].getFuelQuantity());
			while (1) {
				this->selected_tank_id = rand() % (tank.size() - 2) + 1;
				if (this->tank[selected_tank_id].getFuelQuantity() >= this->fuel_per_second) {
					break;
				}
			}
		}
		this->tank[this->selected_tank_id].setFuelQuantity(-this->fuel_per_second);
	}
}
void Engine::start_engine() {
	if (this->tank.size() > 1) {
		this->status = true;
	}
	this->output += "Started\n";
}
void Engine::stop_engine() {
	this->status = false;
	this->output += "Stoped\n";
}
void Engine::give_back_fuel(int quantity) {
	double remain_fuel = this->tank[0].getFuelQuantity();
	int min = 1;
	for (int i = 2; i < this->tank.size(); i++) {
		if (this->tank[min].getFuelQuantity() < this->tank[i].getFuelQuantity())
			min = i;
	}
	this->tank[min].setFuelQuantity(remain_fuel);
}
void Engine::add_fuel_tank(int capacity) {
	Tank addition(capacity);
	this->tank.push_back(addition);
}
void Engine::list_fuel_tanks() {
	for (int i = 0; i < this->tank.size(); i++) {
		this->output += to_string(i) + "\n";
	}
}
void Engine::print_fuel_tank_count() {
	this->output += to_string(this->tank.size()) + "\n";
}
void Engine::remove_fuel_tank(int tank_id) {
	this->tank[tank_id].~Tank();
}
void Engine::connect_fuel_tank_to_engine(int tank_id) {
	if (this->tank[tank_id].getCapacity() != 0 && this->tank.size() > tank_id) {
		this->tank[tank_id].setConnection(true);
	}
	else
		this->output += "There is no such fuel tank at this tank id.\n";
}
void Engine::disconnect_fuel_tank_from_engine(int tank_id) {
	if (this->tank.size() > tank_id) {
		this->tank[tank_id].setConnection(false);
	}
}
void Engine::list_connected_tanks() {
	for (int i = 0; i < tank.size(); i++) {
		if(this->tank[i].getConnection()) this->output += to_string(i) + "\n";
	}
}
void Engine::print_total_fuel_quantity() {
	double sum = 0;
	for (int i = 0; i < tank.size(); i++) {
		sum += this->tank[i].getFuelQuantity();
	}
	this->output += "Total Fuel Quantity : " + to_string(sum) + "\n";
}
void Engine::print_total_consumed_fuel_quantity() {
	double sum = 0;
	for (int i = 0; i < tank.size(); i++) {
		if(this->tank[i].getConnection() == true)
		sum += this->tank[i].getCapacity() - this->tank[i].getFuelQuantity();
	}
	this->output += "Total Consumed Fuel Quantity : " + to_string(sum) + "\n";
}
void Engine::print_tank_info(int tank_id) {
	this->output += this->tank[tank_id].getInfo();
}
void Engine::fill_tank(int tank_id, double fuel_quantity) {
	this->tank[tank_id].setFuelQuantity(fuel_quantity - this->tank[tank_id].getFuelQuantity());
}
void Engine::open_valve(int tank_id) {
	this->tank[tank_id].setValve(true);
}
void Engine::close_valve(int tank_id) {
	this->tank[tank_id].setValve(false);
}
void Engine::break_fuel_tank(int tank_id) {
	this->tank[tank_id].setBroken(true);
}
void Engine::repair_fuel_tank(int tank_id) {
	this->tank[tank_id].setBroken(false);
}
void Engine::wait(int seconds) {
	if (this->status == true) {
		if (this->selected_tank_id == 0 && this->tank[0].getFuelQuantity() >= 20) {
			this->tank[0].setFuelQuantity(-1 * this->fuel_per_second * seconds);
			return;
		}
		else if ((this->selected_tank_id == 0 && this->tank[0].getFuelQuantity() < 20) ||
			(this->tank[selected_tank_id].getFuelQuantity() <= 0)) {
			this->tank[this->selected_tank_id].setFuelQuantity(-this->tank[this->selected_tank_id].getFuelQuantity());
			while (1) {
				this->selected_tank_id = rand() % (this->tank.size() - 2) + 1;
				if (this->tank[this->selected_tank_id].getFuelQuantity() >= this->fuel_per_second) {
					break;
				}
			}
		}
		this->tank[this->selected_tank_id].setFuelQuantity(-1 * this->fuel_per_second * seconds);
	}
}