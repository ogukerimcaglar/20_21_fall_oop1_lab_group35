#pragma once
#include <iostream>
#include <string>
using namespace std;
class Tank {
private:
	double capacity;
	double fuel_quantity;
	bool broken;
	bool valve;
	bool connection;
public:
	Tank(double _capacity);
	~Tank();
	double getCapacity();
	double getFuelQuantity();
	bool getValve();
	bool getConnection();
	void setFuelQuantity(double _fuel_quantity);
	void setBroken(bool isBroken);
	void setValve(bool valveOpen);
	void setConnection(bool _connection);
	string getInfo();
};