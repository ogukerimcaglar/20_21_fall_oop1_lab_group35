#pragma once
#include<iostream>
#include <fstream>
#include <string>
#include "Engine.h"
using namespace std;

class InputFile {
private:
	void commands(string str, Engine& engine);
public:
	void ReadFile(string fileName, Engine& engine);
	void stop_simulation(Engine& engine);
};
