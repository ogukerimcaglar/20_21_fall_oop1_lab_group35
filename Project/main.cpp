#include<iostream>
#include "Engine.h"
#include "Tank.h"
#include "OutputFile.h"
#include "InputFile.h"
using namespace std;

int main() {
	string inputFile = "", outputFile = "";
	InputFile input;
	OutputFile output;
	Engine engine;
	cout << "Enter Input File Name : ";
	cin >> inputFile;
	cout << "Enter Output File Name : ";
	cin >> outputFile;

	input.ReadFile(inputFile, engine);

	output.to_write = engine.output;
	output.WriteFile(outputFile);

	system("pause");
	return 0;
}